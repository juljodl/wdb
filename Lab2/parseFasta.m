function [fastaStruct] = parseFasta(fastaFile)

fastaStruct = struct('id',{}, 'sequence',{});
dataFasta = char(fastaFile);
delimiter = sprintf('\n');
n = 0;
m = 0;
    for i = 1:length(dataFasta)
        if dataFasta(i) == '>'
          n = n+1; 
        end
    end

if n > 0
    if n > 1
        tempTab = strsplit(dataFasta,'>');
        tempTab(1) = [];
            for j = 1:n
            [opis,tempTab(j)] = strtok(tempTab(j),delimiter);
            m = m+1;
            fastaStruct(m).id = opis;
            tempTab(j) = erase(tempTab(j),delimiter);
            fastaStruct(m).sequence = tempTab(j);
            end 
    elseif n == 1
        [opis,dataFasta] = strtok(dataFasta,delimiter);
        fastaStruct(n).id = opis(2:end);
        dataFasta = erase(dataFasta,delimiter);
        fastaStruct(n).sequence = dataFasta;
    end
    
end
end

