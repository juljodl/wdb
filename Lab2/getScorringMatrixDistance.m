function [seq1,seq2,M] = getScorringMatrix(seq1, seq2,cgap, cmis)
n = length(seq1);
m = length(seq2);
if n < m
  x = n; 
  n = m;
  m = x;
  y = seq1;
  seq1 = seq2;
  seq2 = y;
end

M = zeros(n,m);


ngap = cgap;
mgap = cgap;
for i = 1:n-1
    for j = 1:m-1
        if i == 1 
        M(i,j+1) = ngap;
        ngap = ngap + 1;
        end
        if j == 1
            M(i+1,j) = mgap;
            mgap = mgap+1;
        end
    end
end


for i = 2:n 
    for j = 2:m
        del = M(i-1,j) + cgap ;
        ins = M(i,j-1) + cgap;
        diag = M(i-1,j-1) + cmis;
            if seq1(i) == seq2(j)
                diag = M(i-1,j-1);
            end
            if (diag <= ins && diag <= del)
                M(i,j) = diag;
            elseif (del < ins)
                M(i,j) = del;
            else
                M(i,j) = ins;
            end
    end
end

end