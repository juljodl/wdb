function [traceMatrix,nseq1,nseq2,dl,ngap,nmis,nmatch] = getTraceBackSimilar(seq1,seq2,ScMatrix)
nseq1 = '';
nseq2 = '';
i = size(ScMatrix,1);
j = size(ScMatrix,2);
dl = 0;
ngap = 0;
nmis = 0;
nmatch = 0;
while i >= 1 && j >=1
            
        if i==1 && j==1
               traceMatrix(1,1) = 1;
               i = i-1;
               j = j-1;
        elseif j==1 
            traceMatrix(i,j) = 1;
            i = i - 1;
            ngap = ngap + 1;
            dl = dl + 1;
        elseif i==1 
            traceMatrix(i,j) = 1;
            j = j-1;
            ngap = ngap + 1;
            dl = dl + 1;
        else
            del = ScMatrix(i-1,j);
            ins = ScMatrix(i,j-1);
            diag = ScMatrix(i-1,j-1);
            if (diag>=ins && diag>=del)
                if seq1(i)==seq2(j)
                    nmatch = nmatch + 1;
                else
                    nmis = nmis + 1;
                end
                nseq1 = strcat(seq1(i),nseq1);
                nseq2 = strcat(seq2(j),nseq2);
                traceMatrix(i,j) = 1;
                i = i - 1;
                j = j - 1;
                dl = dl + 1;
            elseif ins >= del 
                nseq1 = strcat(seq1(i),nseq1);
                nseq2 = strcat('-',nseq2);
                traceMatrix(i,j) = 1;
                i = i - 1;
                dl = dl + 1;
                ngap = ngap + 1;
            else 
                nseq1 = strcat('-',nseq1);
                nseq2 = strcat(seq2(j),nseq2);
                traceMatrix(i,j) = 1;
                j = j - 1;
                dl = dl + 1;
                ngap = ngap + 1;
        
            end
        end


end