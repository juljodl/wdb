function seqMatrix = getMatrixData(seq1,seq2)

len1=length(seq1);
len2=length(seq2);

    if len1>len2
        x = len1;
        len1 = len2;
        len2 = x;
        y = seq1;
        seq1 = seq2;
        seq2 = y;
    end

seqMatrix = zeros(len1,len2);

for i = 2 : len1
    for j = 2 : len2
        if seq1(i) == seq2(j)
            seqMatrix(i,j) = 1;
        end
    end
end

end 