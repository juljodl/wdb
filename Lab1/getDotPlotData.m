function newMatrix = getDotPlotData(seqMatrix, window, threshold)

n = size(seqMatrix,1);
m = size(seqMatrix,2);

tempWin = zeros(window);
newMatrix = zeros(n,m);
    for i = 1 : n - window
        for j = 1 : m - window
            tempWin = seqMatrix(i : i+(window-1), j : j+(window-1));
            if (trace(tempWin) >= (window - threshold))
            diagWin = diag(tempWin);
            newMatrix(i:i+(window-1),j:j+(window-1))= diag(diagWin);              
            end
        end 

    end
end
