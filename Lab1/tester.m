clear all
plik=('przyklad2.fasta');
wczytanyPlik = readFile(plik);
fastaStruct = parseFasta(wczytanyPlik);
seq1=fastaStruct(1).sequence;
seq1=char(seq1);
seq2=fastaStruct(2).sequence;
seq2=char(seq2);
seqMatrix =getMatrixData(seq1,seq2);
filteredMatrix=getDotPlotData(seqMatrix,7,0);
dotPlot = showDotPlot(filteredMatrix);