function dotplot = showDotPlot(filteredMatrix)
figure;
dotplot = imagesc(filteredMatrix);
len11=size(filteredMatrix,1);
len22=size(filteredMatrix,2);
xlim=([len11]);
ylim=([len22]);
colormap (1-gray); 
title('Filtrowana macierz kropkowa');
end