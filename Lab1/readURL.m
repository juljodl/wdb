function fastaFile = readURL(id)
url = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi';
fastaFile = urlread(url, 'get', {'db', 'nucleotide','rettype', 'fasta','id', id});
end