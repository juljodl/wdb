function [traceMatrix,seq1Tab,seq2Tab,longestWay,x,y,xend,yend] = getTraceBack(seq1,seq2,ScMatrix)

n = size(ScMatrix,1);
m = size(ScMatrix,2);
maxValue = max(ScMatrix(:));
[x,y]=find(ScMatrix==maxValue);
seq1Tab = [];
seq2Tab = [];
xend = [];
yend = [];
dlStart = 1 ; 
dl = 1;

for k = 1:length(x) 
    nseq1 = '';
    nseq2 = '';
    i = x(k);
    j = y(k);
    nseq1 = strcat(seq1(i),nseq1);
    nseq2 = strcat(seq2(j),nseq2);
    traceMatrix(i,j) = 1;
    del = ScMatrix(i-1,j);
    ins = ScMatrix(i,j-1);
    diag = ScMatrix(i-1,j-1);
    while (diag >0 || ins>0 || del >0)
           if diag >0
               i = i - 1;
               j = j - 1;
               nseq1 = strcat(seq1(i),nseq1);
               nseq2 = strcat(seq2(j),nseq2);
               traceMatrix(i,j) = 1;
               Way(i,j) = 1;
               dl = dl + 1;
           elseif ins > 0
               j = j - 1;
               nseq1 = strcat(seq1(i),nseq1);
               nseq2 = strcat('-',nseq2);
               traceMatrix(i,j) = 1;
               Way(i,j) = 1;
               dl = dl + 1;
           elseif del > 0
               i = i - 1;
               nseq1 = strcat('-',nseq1);           
               nseq2 = strcat(seq2(j),nseq2);
               traceMatrix(i,j) = 1;
               Way(i,j) = 1;
               dl = dl + 1;
           end %if
           del = ScMatrix(i-1,j);
           ins = ScMatrix(i,j-1);
           diag = ScMatrix(i-1,j-1);
    end %while
        xend = [xend i];
        yend = [yend j];
        nseq1=convertCharsToStrings(nseq1);
        nseq2=convertCharsToStrings(nseq2);
        seq1Tab = [seq1Tab nseq1];
        seq2Tab = [seq2Tab nseq2];
        
    if dl > dlStart
        longestWay = Way;
        dlStart = dl;
        dl = 1;
        Way = [];    
    end
end %for k
 
end %function
    

    



