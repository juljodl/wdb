function [seq1,seq2,M] = getScorringMatrix(seq1, seq2,subMatrix,cgap,seqType)

n = length(seq1);
m = length(seq2);

if seqType == 1
    nitroBases = ['A', 'C','G','T']; 
else
    nitroBases = ['A', 'C','G','U']; 
end

if n < m
  x = n; 
  n = m;
  m = x;
  y = seq1;
  seq1 = seq2;
  seq2 = y;
end

M = zeros(n,m);


for i = 2:n 
    for j = 2:m
        del = M(i-1,j) + cgap;
        ins = M(i,j-1) + cgap;
        k = 1;
        l = 1;
        while seq1(i) ~= nitroBases(k)
            k = k + 1;
        end
        while seq2(j) ~= nitroBases(l)
            l = l + 1;
        end  
        diag = M(i-1,j-1) + subMatrix(k,l);
        M(i,j) = max([del,ins,diag,0]);     
        
    end
         
end

end

