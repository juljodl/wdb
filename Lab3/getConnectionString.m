function str = getConnectionString(seq1, seq2)
n = length(seq1);
str = '';
i = 1;
while i <= n 
        if seq1(i)==seq2(i)
            str = strcat(str,'|');
            i = i + 1;
        elseif seq1(i)~= seq2(i)
            str = strcat(str,'x');
            i = i + 1;
        end
end    
 
 end